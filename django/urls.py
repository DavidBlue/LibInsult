#
# Copyright (C) 2016-2022 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.conf.urls import url

from . import views


formats = "|".join(fmt.name for fmt in views.output_formats)

app_name="insult"
urlpatterns = [
    url(r'^$', views.help, name="help"),
    url(
        r'^insult\.(?P<format>%s)' % formats,
        views.FormatDispatcher.as_view(),
        name="api"
    ),
    url(
        r'^(?P<lang>[a-zA-Z_]+)/insult\.(?P<format>%s)' % formats,
        views.FormatDispatcher.as_view(),
        name="api_lang"
    ),
    url(
        r'^insult',
        views.TextFormat.as_view(),
        name="api_txt"
    ),
    url(
        r'^adjective\.(?P<format>%s)' % formats,
        views.FormatDispatcher.as_view(
            default_template="<adjective>",
            allow_override_template=False,
        ),
        name="api_adjective"
    ),
    url(
        r'^(?P<lang>[a-zA-Z_]+)/adjective\.(?P<format>%s)' % formats,
        views.FormatDispatcher.as_view(
            default_template="<adjective>",
            allow_override_template=False,
        ),
        name="api_adjective_lang"
    ),
    url(
        r'^adjective',
        views.TextFormat.as_view(
            default_template="<adjective>",
            allow_override_template=False,
        ),
        name="api_adjective_txt"
    ),
    url(r'^explore$', views.explore, name="explore"),
]
